/*
   Samba Unix/Linux SMB client library
   Helper lib to create an udp event and write it to an buffer
   Copyright (C) 2019 Simon Muras (s.muras@laudert.com)

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "replace.h"

#define UDP_PACKET_HEADER  "smb_audit"
#define UDP_MAXPATHLEN  4096
#define UDP_MAXBUF 16384

/*
 * reference udp struct, max buffer UDP_MAXBUF, each path shortened to UDP_MAXPATHLEN
 * udp_packet_header = "smb_audit"
 * udp_event_type = SMB_VFS_OP_CONNECT = 0,...,SMB_VFS_OP_LAST
 * udp_event_id = consecutive event number
 * udp_success = has the file operation been successful
 * udp_message_len + udp_message = success/failure message
 * udp_info1len + udp_info1 = usually the fully qualified file name (except connect, disconnect)
 * udp_info2len + udp_info2 = usually empty, for rename the target file name
 */

struct udp_event {
	char          udp_packet_header[9];
	unsigned char udp_event_type;
	uint32_t      udp_event_id;
	unsigned char udp_success;
	uint16_t      udp_message_len;
	char          udp_message;
	uint16_t      udp_info1len;
	char          udp_info1[UDP_MAXPATHLEN];
	uint16_t      udp_info2len;
	char          udp_info2[UDP_MAXPATHLEN];
};


static ssize_t check_and_increment(ssize_t value, size_t increment)
{
	if ((SSIZE_MAX - value) > increment){
		return value + increment;
	}

	return SSIZE_MAX;
}

static ssize_t build_and_write_udp_event(char *buffer, int event, bool success,
 					const char *message, const char *info1,
 					const char *info2, int event_id)
{
	char *p = buffer;
	size_t infolen;
	ssize_t datalen = 0;
	uint16_t uint16;
	uint32_t uint32;
	memcpy(p, UDP_PACKET_HEADER, 9);
	p += 9;
	datalen = check_and_increment(datalen, 9);

	*p = event;
	p += 1;
	datalen += 1;
	uint32 = event_id;
	uint32 = htonl(uint32);
	memcpy(p, &uint32, sizeof(uint32));
	p += sizeof(uint32);
	datalen = check_and_increment(datalen, sizeof(uint32));

	*p = success;
	p += 1;
	datalen = check_and_increment(datalen, 1);

	infolen = strlen(message);
	uint16 = infolen;
	uint16 = htons(uint16);
	memcpy(p, &uint16, sizeof(uint16));
	p += sizeof(uint16);
	datalen = check_and_increment(datalen, sizeof(uint16));
	memcpy(p, message, infolen);
	p += infolen;
	datalen = check_and_increment(datalen, infolen);

	if (info1) {
		if ((infolen = strlen(info1)) >= UDP_MAXPATHLEN){
		 	infolen = UDP_MAXPATHLEN - 1;
		}

		uint16 = infolen;
		uint16 = htons(uint16);
		memcpy(p, &uint16, sizeof(uint16));
		p += sizeof(uint16);
		datalen = check_and_increment(datalen, sizeof(uint16));
		memcpy(p, info1, infolen);
		p += infolen;
		datalen = check_and_increment(datalen, infolen);
	} else {
		uint16 = htons(0);
		memcpy(p, &uint16, sizeof(uint16));
		p += sizeof(uint16);
		datalen = check_and_increment(datalen, sizeof(uint16));
	}

	if (info2) {
		if ((infolen = strlen(info2)) >= UDP_MAXPATHLEN){
			infolen = UDP_MAXPATHLEN - 1;
		}

		uint16 = infolen;
		uint16 = htons(uint16);
		memcpy(p, &uint16, sizeof(uint16));
		p += sizeof(uint16);
		datalen = check_and_increment(datalen, sizeof(uint16));
		memcpy(p, info2, infolen);
		p += infolen;
		datalen = check_and_increment(datalen, infolen);
	} else {
		uint16 = htons(0);
		memcpy(p, &uint16, sizeof(uint16));
		p += sizeof(uint16);
		datalen = check_and_increment(datalen, sizeof(uint16));
	}

	return datalen;
}







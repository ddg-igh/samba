/*
 * Auditing VFS module for samba. Log selected file operations to an udp server.
 *
 * Copyright (C) Simon Muras, 2019
 * Copyright (C) René Bock, 2021
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

/* This module is based on a merge of the vfs_full_audit and vfs_audit modules.
 * This module implements logging to an udp server for a modified set of the
 * Samba VFS operations in the vfs_audit.module.
 *
 * You use it as follows:
 *
 * [tmp]
 * path = /tmp
 * vfs objects = udp_audit
 * udp_audit:success = mkdir,rmdir,close_write,rename_dir,rename_file,unlink,create_file
 * udp_audit:failure = none
 * udp_audit:host = localhost
 * udp_audit:port = 12250
 * udp_audit:ignored_files=.DS_Store
 * udp_audit:ignored_dirs=check_mount
 *
 * vfs op can be "all" which means log all supported operations.
 * vfs op can be "none" which means no logging.
 *
 * Options:
 *
 * success: A list of VFS operations for which a successful completion should
 * be logged. Defaults to no logging at all. The special operation "all" logs
 * all supported operations.
 * The following operations are supported:
 * connect, disconnect, opendir, mkdir, rmdir, open_read, open_write, create_file,
 * close_read, close_write, rename_dir, rename_file, unlink, chmod, fchmod
 *
 * failure: A list of VFS operations for which failure to complete should be
 * logged. Defaults to logging everything.
 *
 * host,port: The server and port to which the udp package is sent to.
 *
 * ignored_files:
 * To prevent udp events for unnecessary file operations you can specify a "/"
 * separated lists of file names.
 * If a file name equals a filename in this list no events are thrown.
 *
 * ignored_dirs:
 * To prevent udp events for unnecessary file operations in directories you can
 * specify a "/" separated lists of directory names.
 * If a file path contains a directory in this list no events are thrown.
 *
 * See net_udp_event.c for a description of the udp event.
 *
 */


#include "includes.h"
#include "system/filesys.h"
#include "system/syslog.h"
#include "smbd/smbd.h"
#include "smbd/fd_handle_private.h"
#include "../librpc/gen_ndr/ndr_netlogon.h"
#include "auth.h"
#include "ntioctl.h"
#include "lib/param/loadparm.h"
#include "lib/util/bitmap.h"
#include "lib/util/tevent_unix.h"
#include "libcli/security/sddl.h"
#include "passdb/machine_sid.h"
#include "lib/util/tevent_ntstatus.h"
#include "../utils/net_udp_event.c"
#include "lib/util/string_wrappers.h"

/* struct for the current initialized udp connection */
struct udp_connection {
	int sock;
	struct addrinfo addrinfo;
	struct sockaddr_storage sockaddr;
	time_t next_try_on_error;
};

static bool udp_initialized = false;
static struct udp_connection udp_connection;

/* helper struct to save the currently open file descriptors with writing access */
struct writing_fd_entry {
	struct writing_fd_entry *prev, *next;
	int fd;
};

struct vfs_upd_audit_private_data {
	struct bitmap *success_ops;
	struct bitmap *failure_ops;
	const char *udp_host;
	const char *udp_port;
	int event_id;
	struct writing_fd_entry* write_fds;
	const char **ignored_dirs;
	const char **ignored_files;
};


typedef enum vfs_op_type {
	SMB_VFS_OP_CONNECT = 0,
	SMB_VFS_OP_DISCONNECT,
	SMB_VFS_OP_OPENDIR,
	SMB_VFS_OP_MKDIR,
	SMB_VFS_OP_RMDIR,
	SMB_VFS_OP_OPEN_READ,
	SMB_VFS_OP_OPEN_WRITE,
	SMB_VFS_OP_CREATE_FILE,
	SMB_VFS_OP_CLOSE_READ,
	SMB_VFS_OP_CLOSE_WRITE,
	SMB_VFS_OP_RENAME_DIR,
	SMB_VFS_OP_RENAME_FILE,
	SMB_VFS_OP_UNLINK,
	SMB_VFS_OP_CHMOD,
	SMB_VFS_OP_FCHMOD,
	SMB_VFS_OP_LAST
} vfs_op_type;


static struct {
	vfs_op_type type;
	const char *name;
} vfs_op_names[] = {
	{ SMB_VFS_OP_CONNECT,	"connect" },
	{ SMB_VFS_OP_DISCONNECT,	"disconnect" },
	{ SMB_VFS_OP_OPENDIR,	"opendir" },
	{ SMB_VFS_OP_MKDIR,	"mkdir" },
	{ SMB_VFS_OP_RMDIR,	"rmdir" },
	{ SMB_VFS_OP_OPEN_READ,	"open_read" },
	{ SMB_VFS_OP_OPEN_WRITE,	"open_write" },
	{ SMB_VFS_OP_CREATE_FILE, "create_file"},
	{ SMB_VFS_OP_CLOSE_READ,	"close_read" },
	{ SMB_VFS_OP_CLOSE_WRITE,	"close_write" },
	{ SMB_VFS_OP_RENAME_DIR,	"rename_dir" },
	{ SMB_VFS_OP_RENAME_FILE,	"rename_file" },
	{ SMB_VFS_OP_UNLINK,	"unlink" },
	{ SMB_VFS_OP_CHMOD,	"chmod" },
	{ SMB_VFS_OP_FCHMOD,	"fchmod" },
	{ SMB_VFS_OP_LAST, NULL }
};

//udp buffer
static char buffer[UDP_MAXBUF];

//retry delay after a socket has been closed
#define UDP_RETRY_DELAY_S 600

//helper method to determine if a file is open for writing
static struct writing_fd_entry *ldb_find_writing_fd(struct vfs_upd_audit_private_data* data,
 						   int fd)
{
	struct writing_fd_entry *writing_fd;
	for (writing_fd = data->write_fds;
	     writing_fd;
	     writing_fd = writing_fd->next) {
		if (writing_fd->fd == fd) {
			return writing_fd;
		}
	}
	return NULL;
}

//see vfs_full_audit
static bool log_success(struct vfs_upd_audit_private_data *pd, vfs_op_type op)
{
	if (pd->success_ops == NULL) {
		return true;
	}

	return bitmap_query(pd->success_ops, op);
}

//see vfs_full_audit
static bool log_failure(struct vfs_upd_audit_private_data *pd, vfs_op_type op)
{
	if (pd->failure_ops == NULL)
		return true;

	return bitmap_query(pd->failure_ops, op);
}

//see vfs_full_audit
static struct bitmap *init_bitmap(TALLOC_CTX *mem_ctx, const char **ops)
{
	struct bitmap *bm;

	if (ops == NULL) {
		return NULL;
	}

	bm = bitmap_talloc(mem_ctx, SMB_VFS_OP_LAST);
	if (bm == NULL) {
		DBG_INFO("Could not alloc bitmap -- "
			  "defaulting to logging everything\n");
		return NULL;
	}

	for (; *ops != NULL; ops += 1) {
		int i;
		bool neg = false;
		const char *op = NULL;

		if (strequal(*ops, "all")) {
			for (i=0; i<SMB_VFS_OP_LAST; i++) {
				bitmap_set(bm, i);
			}
			continue;
		}

		if (strequal(*ops, "none")) {
			break;
		}

		op = ops[0];
		if (op[0] == '!') {
			neg = true;
			op += 1;
		}

		for (i=0; i<SMB_VFS_OP_LAST; i++) {
			if ((vfs_op_names[i].name == NULL)
			 || (vfs_op_names[i].type != i)) {
				smb_panic("vfs_udp_audit.c: name table not in sync with vfs_op_type enums\n");
			}
			if (strequal(op, vfs_op_names[i].name)) {
				if (neg) {
					bitmap_clear(bm, i);
				} else {
					bitmap_set(bm, i);
				}
				break;
			}
		}
		if (i == SMB_VFS_OP_LAST) {
			DBG_INFO("Could not find opname %s, logging all\n",
				  *ops);
			TALLOC_FREE(bm);
			return NULL;
		}
	}
	return bm;
}

/* opens an udp connection */
static bool init_udp(struct vfs_upd_audit_private_data *pd)
{
	struct addrinfo *servinfo, *p;
	struct addrinfo hints;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
	udp_connection.next_try_on_error = 0;

	if (udp_initialized){
		return true;
	}

	if ((getaddrinfo(pd->udp_host, pd->udp_port, &hints, &servinfo)) == 0) {
		for (p = servinfo; p != NULL; p = p->ai_next) {
			udp_connection.sock = socket(p->ai_family,
			 			     p->ai_socktype,
			 			     p->ai_protocol);
			if (udp_connection.sock != -1) {
				break;
			}
		}
		if (p != NULL){
			memcpy(&udp_connection.addrinfo, p,
			       sizeof(struct addrinfo));
			memcpy(&udp_connection.sockaddr, p->ai_addr,
			       sizeof(struct sockaddr_storage));
		}

		freeaddrinfo(servinfo);
		return true;
	}

	return false;
}

static TALLOC_CTX *tmp_do_log_ctx;

//see vfs_full_audit
static TALLOC_CTX *do_log_ctx(void)
{
	if (tmp_do_log_ctx == NULL) {
		tmp_do_log_ctx = talloc_named_const(NULL, 0, "do_log_ctx");
	}
	return tmp_do_log_ctx;
}

/* helper method to determine if a string s ends with another string */
static int strend(const char *s, const char *t)
{
	size_t ls = strlen(s);
	size_t lt = strlen(t);
	if (ls >= lt) {
		return (0 == memcmp(t, s + (ls - lt), lt));
	}
	return 0;
}

/* helper method to determine if a file has to be ignored for a udp event */
static bool fsp_is_ignored(struct vfs_upd_audit_private_data *pd,
			   const char *full_path)
{
	const char** iter=NULL;

	if (pd->ignored_files){
		for (iter=pd->ignored_files; *iter != NULL; iter += 1) {
			char *pattern = talloc_strdup(do_log_ctx(), "/");
			pattern = talloc_strdup_append_buffer (pattern, *iter);
			if (strend(full_path, pattern)) {
				return true;
			}
		}
	}

	if (pd->ignored_dirs){
		for (iter=pd->ignored_dirs; *iter != NULL; iter += 1) {
			char *pattern = talloc_strdup(do_log_ctx(), "/");
			pattern = talloc_strdup_append_buffer (pattern, *iter);
			pattern = talloc_strdup_append_buffer (pattern, "/");
			if (strstr(full_path, pattern)) {
				return true;
			}
		}
	}

	return false;
}


/* see vfs_full_audit and comments */
static void do_log(vfs_op_type op, bool success, struct vfs_handle_struct *handle,
		   const char *info1, bool info1_is_file, const char *info2,
		   bool info2_is_file,  int fd)
{
	struct vfs_upd_audit_private_data *pd = NULL;
	struct writing_fd_entry* entry = NULL;
	fstring err_msg;

	time_t now = time(NULL);
	ssize_t data_len;
	int sent_data = 0;

	SMB_VFS_HANDLE_GET_DATA(handle, pd,
	 			struct vfs_upd_audit_private_data, return;);

	/* if a file is created or opened with write access we store the file descriptor */
	if (op == SMB_VFS_OP_OPEN_WRITE || op == SMB_VFS_OP_CREATE_FILE) {
		entry = talloc_zero(pd, struct writing_fd_entry);
		entry->fd = fd;
		DLIST_ADD_END(pd->write_fds, entry);
	}

	/* if we have a close operation we check for a writing close */
	if (op == SMB_VFS_OP_CLOSE_READ) {
		entry=ldb_find_writing_fd(pd, fd);
		if (entry){
			op = SMB_VFS_OP_CLOSE_WRITE;
			DLIST_REMOVE(pd->write_fds, entry);
			TALLOC_FREE(entry);
		}
	}

	if (success && (!log_success(pd, op))){
		goto out;
	}

	if (!success && (!log_failure(pd, op))){
		goto out;
	}

	/* check if the file (source or target file for rename) is ignored */
	if (info1 && info1_is_file && fsp_is_ignored(pd, info1)){
		goto out;
	}

	if (info2 && info2_is_file && fsp_is_ignored(pd, info2)){
    		goto out;
    	}

	if (success){
		fstrcpy(err_msg, "ok");
	}
	else {
		fstr_sprintf(err_msg, "fail (%s)", strerror(errno));
	}

	/* if the udp connection is closed try to reestablish */
	if (udp_connection.sock == -1) {
		if (now < udp_connection.next_try_on_error){
			goto out;
		}

		udp_connection.sock = socket(udp_connection.addrinfo.ai_family,
					     udp_connection.addrinfo.ai_socktype,
					     udp_connection.addrinfo.ai_protocol);

		if (udp_connection.sock == -1) {
			udp_connection.next_try_on_error = now + UDP_RETRY_DELAY_S;
			goto out;
		}

		udp_connection.next_try_on_error = 0;
	}

	data_len = build_and_write_udp_event(buffer, op, success, err_msg, info1,
					     info2, pd->event_id);

	/* send data to udp */
	sent_data = sendto(udp_connection.sock, buffer, data_len, 0,
			   (struct sockaddr *)&udp_connection.sockaddr,
			   udp_connection.addrinfo.ai_addrlen);

	/* check if the send operation has been successfull */
	if (sent_data != data_len) {
		close( udp_connection.sock );
		udp_connection.sock = -1;
		udp_connection.next_try_on_error = now + UDP_RETRY_DELAY_S;
	}

	pd->event_id++;

	/* store private data since event_id or write_fds might have been modified */
	out:
		SMB_VFS_HANDLE_SET_DATA(handle, pd, NULL,
					struct vfs_upd_audit_private_data,
					return);
		TALLOC_FREE(tmp_do_log_ctx);
}


static const char *smb_fname_str_do_log(struct connection_struct *conn,
				const struct smb_filename *smb_fname)
{
	char *fname = NULL;
	NTSTATUS status;

	if (smb_fname == NULL) {
		return "";
	}

	if (smb_fname->base_name[0] != '/') {
		char *abs_name = NULL;
		struct smb_filename *fname_copy = cp_smb_filename(
							do_log_ctx(),
							smb_fname);
		if (fname_copy == NULL) {
			return "";
		}

		if (!ISDOT(smb_fname->base_name)) {
			abs_name = talloc_asprintf(do_log_ctx(),
					"%s/%s",
					conn->cwd_fsp->fsp_name->base_name,
					smb_fname->base_name);
		} else {
			abs_name = talloc_strdup(do_log_ctx(),
					conn->cwd_fsp->fsp_name->base_name);
		}
		if (abs_name == NULL) {
			return "";
		}

		fname_copy->base_name = abs_name;
		smb_fname = fname_copy;
	}

	status = get_full_smb_filename(do_log_ctx(), smb_fname, &fname);
	if (!NT_STATUS_IS_OK(status)) {
		return "";
	}
	return fname;
}


static const char *fsp_str_do_log(const struct files_struct *fsp)
{
	return smb_fname_str_do_log(fsp->conn, fsp->fsp_name);
}



/*
 * smb_udp_audit_connect loads the ignored files, ignored dirs, udp config
 * and establishes the udp connection
 */
static int smb_udp_audit_connect(vfs_handle_struct *handle,
			 const char *svc, const char *user)
{
	int result;
	const char *none[] = { "none" };
	struct vfs_upd_audit_private_data *pd = NULL;

	result = SMB_VFS_NEXT_CONNECT(handle, svc, user);
	if (result < 0) {
		return result;
	}

	pd = talloc_zero(handle, struct vfs_upd_audit_private_data);
	if (!pd) {
		SMB_VFS_NEXT_DISCONNECT(handle);
		return -1;
	}

	pd->success_ops = init_bitmap(pd,
				      lp_parm_string_list(SNUM(handle->conn),
	 			      "udp_audit", "success", none));

	pd->failure_ops = init_bitmap(pd,
				      lp_parm_string_list(SNUM(handle->conn),
	 			      "udp_audit", "failure", none));

	pd->ignored_dirs = str_list_make_v3_const(pd,
						  lp_parm_const_string(SNUM(handle->conn),
						  "udp_audit", "ignored_dirs", NULL), "/");

	pd->ignored_files = str_list_make_v3_const(pd,
						   lp_parm_const_string(SNUM(handle->conn),
						   "udp_audit", "ignored_files", NULL), "/");

	pd->udp_port = lp_parm_const_string(SNUM(handle->conn),
					    "udp_audit", "port", NULL);

	pd->udp_host = lp_parm_const_string(SNUM(handle->conn),
					    "udp_audit", "host", NULL);
	pd->event_id = 1;

	if (!pd->udp_host || !pd->udp_port || !init_udp(pd)) {
		SMB_VFS_NEXT_DISCONNECT(handle);
		return -1;
	}

	/* Store the private data. */
	SMB_VFS_HANDLE_SET_DATA(handle, pd, NULL,
				struct vfs_upd_audit_private_data, return -1);

	do_log(SMB_VFS_OP_CONNECT, true, handle, svc, false, NULL, false, 0);

	return 0;
}



//see vfs_full_audit
static void smb_udp_audit_disconnect(vfs_handle_struct *handle)
{
	const struct loadparm_substitution *lp_sub = loadparm_s3_global_substitution();

	SMB_VFS_NEXT_DISCONNECT(handle);
	do_log(SMB_VFS_OP_DISCONNECT, true, handle,
	       lp_servicename(talloc_tos(), lp_sub, SNUM(handle->conn)),
	       false, NULL, false, 0);
}



/* all directory accesses are relative, but we want to receive the full path */
static DIR *smb_udp_audit_fdopendir(vfs_handle_struct *handle,
			files_struct *fsp,
			const char *mask,
			uint32_t attr)
{
	DIR *result = NULL;

	result = SMB_VFS_NEXT_FDOPENDIR(handle, fsp, mask, attr);

	do_log(SMB_VFS_OP_OPENDIR, (result != NULL), handle,
	       fsp_str_do_log(fsp), true, NULL, false, 0);

	return result;
}

/* all directory accesses are relative, but we want to receive the full path */
static int smb_udp_audit_mkdirat(vfs_handle_struct *handle,
				 struct files_struct *dirfsp,
				 const struct smb_filename *smb_fname,
				 mode_t mode)
{
	struct smb_filename *full_fname = NULL;
	int result;

	full_fname = full_path_from_dirfsp_atname(talloc_tos(),
						  dirfsp,
						  smb_fname);

	if (full_fname == NULL) {
		return -1;
	}

	result = SMB_VFS_NEXT_MKDIRAT(handle,dirfsp, smb_fname, mode);

	do_log(SMB_VFS_OP_MKDIR, (result >= 0), handle,
	       smb_fname_str_do_log(handle->conn, full_fname),
	       true, NULL, false, 0);

	TALLOC_FREE(full_fname);
	return result;
}

static int smb_udp_audit_rmdir(vfs_handle_struct *handle,
			       struct files_struct *dirfsp,
			       const struct smb_filename *smb_fname,
			       int flags)
{
	struct smb_filename *full_fname = NULL;
	int result;

	full_fname = full_path_from_dirfsp_atname(talloc_tos(),
						  dirfsp,
						  smb_fname);

	if (full_fname == NULL) {
		return -1;
	}

	result = SMB_VFS_NEXT_UNLINKAT(handle, dirfsp, smb_fname, flags);

	do_log(SMB_VFS_OP_RMDIR, (result >= 0), handle,
	       smb_fname_str_do_log(handle->conn, full_fname),
	       true, NULL, false, 0);

	TALLOC_FREE(full_fname);

	return result;
}

/* We first check for a open operation if the file already exists,
 * if not we log SMB_VFS_OP_CREATE_FILE.
 * If the file exists we check if the file is opened for read or write access.
 * to be able to differentiate writing and reading close operations later
 * We also communicate the filedescriptor held by result
 */

static int smb_udp_audit_openat(vfs_handle_struct *handle,
				const struct files_struct *dirfsp,
				const struct smb_filename *smb_fname,
				struct files_struct *fsp,
				int flags, mode_t mode)
{
	int result;
	struct smb_filename *fname_tmp = NULL;
	struct smb_filename *full_fname = NULL;
	bool exists;
	vfs_op_type op;

	full_fname = full_path_from_dirfsp_atname(talloc_tos(),
						  dirfsp,
						  smb_fname);

	if (full_fname == NULL) {
		return -1;
	}


	fname_tmp = cp_smb_filename(talloc_tos(), smb_fname);;
	exists = NT_STATUS_IS_OK(vfs_file_exist(handle-> conn, fname_tmp));

	TALLOC_FREE(fname_tmp);

	result = SMB_VFS_NEXT_OPENAT(handle, dirfsp, smb_fname, fsp, flags,
				     mode);

	op = SMB_VFS_OP_OPEN_READ;

	if ((flags & O_WRONLY) || (flags & O_RDWR)){
		op = exists ? SMB_VFS_OP_OPEN_WRITE : SMB_VFS_OP_CREATE_FILE;
	}

	do_log(op, (result >= 0), handle,
	       smb_fname_str_do_log(handle->conn, full_fname),
	       true, NULL, false, result);

	TALLOC_FREE(full_fname);
	return result;
}

/* to differentiate writing and reading close operations we communicate the filedescriptor */
static int smb_udp_audit_close(vfs_handle_struct *handle, files_struct *fsp)
{
	int result;

	result = SMB_VFS_NEXT_CLOSE(handle, fsp);
	do_log(SMB_VFS_OP_CLOSE_READ, (result >= 0), handle,
	       fsp_str_do_log(fsp), true, NULL, false, fsp->fh->fd);
	return result;
}

/* the normal vfs_audit only communicates a rename */
/* we added a support to differ between file and directory operations */
static int smb_udp_audit_renameat(vfs_handle_struct *handle,
				  files_struct *srcfsp,
				  const struct smb_filename *smb_fname_src,
 				  files_struct *dstfsp,
 				  const struct smb_filename *smb_fname_dst)
{
	struct smb_filename *src_full_fname = NULL;
	struct smb_filename *dst_full_fname = NULL;

	int result;
	vfs_op_type op;
	bool isDir;

	src_full_fname = full_path_from_dirfsp_atname(talloc_tos(),
						  srcfsp,
						  smb_fname_src);

	if (src_full_fname == NULL) {
		return -1;
	}

	dst_full_fname = full_path_from_dirfsp_atname(talloc_tos(),
						  dstfsp,
						  smb_fname_dst);

	if (smb_fname_dst == NULL) {
		TALLOC_FREE(src_full_fname);
		return -1;
	}

	result = SMB_VFS_NEXT_RENAMEAT(handle, srcfsp, smb_fname_src, dstfsp,
	 			       smb_fname_dst);

	isDir = S_ISDIR(smb_fname_src->st.st_ex_mode);
	op = isDir ? SMB_VFS_OP_RENAME_DIR : SMB_VFS_OP_RENAME_FILE;

	do_log(op, (result >= 0), handle,
	       smb_fname_str_do_log(handle->conn, src_full_fname),
	       true, smb_fname_str_do_log(handle->conn, dst_full_fname),
	       true, 0);

	TALLOC_FREE(src_full_fname);
	TALLOC_FREE(dst_full_fname);

	return result;
}

static int smb_udp_audit_unlinkat(vfs_handle_struct *handle,
				  struct files_struct *dirfsp,
				  const struct smb_filename *smb_fname,
				  int flags)
{
	struct smb_filename *full_fname = NULL;



	int result = -1;
	if (flags & AT_REMOVEDIR) {
		result = smb_udp_audit_rmdir(handle, dirfsp, smb_fname, flags);

	} else {
		result = SMB_VFS_NEXT_UNLINKAT(handle,  dirfsp, smb_fname,
					       flags);

		full_fname = full_path_from_dirfsp_atname(talloc_tos(),
							  dirfsp,
							  smb_fname);

		if (full_fname == NULL) {
			return -1;
		}

		do_log(SMB_VFS_OP_UNLINK, (result >= 0), handle,
		       smb_fname_str_do_log(handle->conn, full_fname),
		       true, NULL, false, 0);

	       TALLOC_FREE(full_fname);
	}


	return result;
}

/* see vfs_full_audit */
static int smb_udp_audit_chmod(vfs_handle_struct *handle,
	   		       const struct smb_filename *smb_fname,
	   		       mode_t mode)
{
	int result;

	result = SMB_VFS_NEXT_CHMOD(handle, smb_fname, mode);
	do_log(SMB_VFS_OP_CHMOD, (result >= 0), handle,
	       smb_fname_str_do_log(handle->conn, smb_fname),
	       true, NULL, false, 0);


	return result;
}

/* see vfs_full_audit */
static int smb_udp_audit_fchmod(vfs_handle_struct *handle,
				files_struct *fsp, mode_t mode)
{
	int result;

	result = SMB_VFS_NEXT_FCHMOD(handle, fsp, mode);
	do_log(SMB_VFS_OP_FCHMOD, (result >= 0), handle, fsp_str_do_log(fsp),
	       true, NULL, false, 0);

	return result;
}


static struct vfs_fn_pointers vfs_udp_audit_fns = {

	.connect_fn = smb_udp_audit_connect,
	.disconnect_fn = smb_udp_audit_disconnect,
	.fdopendir_fn = smb_udp_audit_fdopendir,
	.mkdirat_fn = smb_udp_audit_mkdirat,
	.openat_fn = smb_udp_audit_openat,
	.close_fn = smb_udp_audit_close,
	.renameat_fn = smb_udp_audit_renameat,
	.unlinkat_fn = smb_udp_audit_unlinkat,
	.chmod_fn = smb_udp_audit_chmod,
	.fchmod_fn = smb_udp_audit_fchmod
};

static_decl_vfs;
NTSTATUS vfs_udp_audit_init(TALLOC_CTX *ctx)
{
	NTSTATUS ret;
	ret = smb_register_vfs(SMB_VFS_INTERFACE_VERSION,
			       "udp_audit",
			       &vfs_udp_audit_fns);

	if (!NT_STATUS_IS_OK(ret)){
		return ret;
	}

	return ret;
}

#
# Blackbox tests for vfs_udp_audit
#
# Copyright (C) René Bock                    2020
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""Blackbox test for vfs udp audit"""

import os
import socket
import threading
import time
from samba.logger import get_samba_logger
from samba.tests import BlackboxTestCase

logger = get_samba_logger(name=__name__)

PORT = 12250
IP = "10.53.57.35"

VFSTEST_ARGS = "--option=vfsobjects=udp_audit --option=udp_audit:success=mkdir,rmdir,close_write,rename_dir,rename_file,unlink,create_file" \
               " --option=udp_audit:failure=none --option=udp_audit:host=%s --option=udp_audit:port=%d" % (IP, PORT)


class VFSUdpAuditBlackboxTest(BlackboxTestCase):

    def setUp(self):
        super(VFSUdpAuditBlackboxTest, self).setUp()
        self.dir = os.path.abspath(os.getcwd())
        self.messages = []
        self.t = threading.Thread(target=VFSUdpAuditBlackboxTest.udp_server, args=(self,))
        self.t.setDaemon(True)
        self.t.start()
        time.sleep(1)

    def tearDown(self):
        super(BlackboxTestCase, self).tearDown()
        self.socket.close()

    def udp_server(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.bind((IP, PORT))
        while True:
            data, addr = self.socket.recvfrom(1024)
            self.messages.append(data)

    def test(self):
        command = "connect;mkdir test_udp;rename test_udp udp_test;rmdir udp_test"
        self.check_run("vfstest %s -c '%s'" % (VFSTEST_ARGS, command))

        time.sleep(1)

        self.assertTrue(len(self.messages) == 3)
        self.check_message(self.messages[0], 3, self.dir + "/test_udp", "")
        self.check_message(self.messages[1], 11, self.dir + "/test_udp", self.dir + "/udp_test")
        self.check_message(self.messages[2], 4, self.dir + "/udp_test", "")

    def check_message(self, message, operation, path1, path2):
        # char          udp_packet_header[9];
        start = 0
        end = 9
        header = message[start:end].decode("UTF-8")
        self.assertTrue(header == "smb_audit")

        # unsigned char udp_event_type;
        start = end
        end = start + 1
        event_type = int(message[start])
        self.assertTrue(event_type == operation)

        # uint32_t      udp_event_id;
        start = end
        end = start + 4
        event_id = int.from_bytes(message[start:end], "big")
        self.assertTrue(event_id > 0)

        # unsigned char udp_success;
        start = end
        end = start + 1
        success = int(message[start])
        self.assertTrue(success == 1)

        # uint16_t      udp_message_len;
        start = end
        end = start + 2
        message_len = int.from_bytes(message[start:end], "big")

        # char          udp_message;
        start = end
        end = start + message_len
        msg = message[start:end].decode("UTF-8")
        self.assertTrue(msg == "ok")

        # uint16_t      udp_info1len;
        start = end
        end = start + 2
        info1_len = int.from_bytes(message[start:end], "big")
        self.assertTrue(info1_len == len(path1))

        # char          udp_info1[UDP_MAXPATHLEN];
        start = end
        end = start + info1_len
        if info1_len > 0:
            info1 = message[start:end].decode("UTF-8")
            self.assertTrue(info1 == path1)

        # uint16_t      udp_info2len;
        start = end
        end = start + 2
        info2_len = int.from_bytes(message[start:end], "big")
        self.assertTrue(info2_len == len(path2))

        # char          udp_info2[UDP_MAXPATHLEN];
        start = end
        end = start + info2_len
        if info2_len > 0:
            info2 = message[start:end].decode("UTF-8")
            self.assertTrue(info2 == path2)
